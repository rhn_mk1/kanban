/*************************************************************************************
 *  Copyright (C) 2010 by Aleix Pol <aleixpol@kde.org>                               *
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#ifndef KANBANPLASMA_HEADER
#define KANBANPLASMA_HEADER

#include <plasma/popupapplet.h>
#include <KDE/Plasma/Label>
#include <KDE/Plasma/ComboBox>
#include <QGraphicsLinearLayout>

class KIcon;
namespace Ui { class Config; }

class QSettings;
class TasksModel;
class TasksView;

class KanbanPlasmoid : public Plasma::PopupApplet
{
	Q_OBJECT
	public:
		KanbanPlasmoid(QObject *parent, const QVariantList &args);
		~KanbanPlasmoid();
		
		void init();
		virtual QGraphicsWidget* graphicsWidget();
		virtual void createConfigurationInterface(KConfigDialog* parent);
		
	private:
		void addAction(QGraphicsLinearLayout* layout, const KIcon& icon, const QString& text, QObject* recv, const char* slot);
		
		QGraphicsWidget* m_widget;
		QGraphicsLinearLayout* m_layout;
		QSettings* m_settings;
		TasksView* m_view;
		TasksModel* m_model;
		Plasma::ComboBox* m_lists;
		Ui::Config* m_configUi;
		
	public slots:
		void addList();
		void changeCurrentList(const QString& name);
		void sync();
		void configAccepted();
		void removeList();
};

#endif
