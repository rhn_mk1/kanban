/*************************************************************************************                                                          
 *  Copyright (C) 2009 by Aleix Pol <aleixpol@kde.org>                               *                                                          
 *                                                                                   *                                                          
 *  This program is free software; you can redistribute it and/or                    *                                                          
 *  modify it under the terms of the GNU General Public License                      *                                                          
 *  as published by the Free Software Foundation; either version 2                   *                                                          
 *  of the License, or (at your option) any later version.                           *                                                          
 *                                                                                   *                                                          
 *  This program is distributed in the hope that it will be useful,                  *                                                          
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *                                                          
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *                                                          
 *  GNU General Public License for more details.                                     *                                                          
 *                                                                                   *                                                          
 *  You should have received a copy of the GNU General Public License                *                                                          
 *  along with this program; if not, write to the Free Software                      *                                                          
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *                                                          
 *************************************************************************************/

#include "tasksview.h"

#include <QHeaderView>
#include <QKeyEvent>
#include <QDebug>
#include <QMenu>
#include <QDateTime>
#include "tasksmodel.h"

TasksView::TasksView(QWidget* parent) : QTreeView(parent)
{
	header()->hide();
	header()->setDefaultSectionSize(10);
	header()->setSortIndicatorShown(false);
	setRootIsDecorated(false);
	setSortingEnabled(true);
	
	setContextMenuPolicy(Qt::CustomContextMenu);
	
	connect(this, SIGNAL(customContextMenuRequested(QPoint)), SLOT(customContextMenuRequested(QPoint)));
}

void TasksView::mousePressEvent(QMouseEvent* event) 
{
	QTreeView::mousePressEvent(event);
	QModelIndex idx= indexAt(event->pos());
	if (!idx.isValid())
		addTask();
}

void TasksView::keyPressEvent(QKeyEvent* e)
{
	QModelIndexList idxs=selectedIndexes();
	if(idxs.isEmpty())
	{
		QTreeView::keyPressEvent(e);
		return;
	}
	
	QModelIndex current = currentIndex();
	
	switch(e->key())
	{
		case Qt::Key_Backspace:
			edit(current);
			break;
		case Qt::Key_Down:
			if(state()!=QAbstractItemView::EditingState && current.row()+1>=model()->rowCount())
				addTask();
			else
				QTreeView::keyPressEvent(e);
			break;
		case Qt::Key_Right:
			if(state()!=QAbstractItemView::EditingState)
				edit(current);
			else
				QTreeView::keyPressEvent(e);
			break;
		case Qt::Key_Tab:
		case Qt::Key_T:
			foreach(const QModelIndex& idx, idxs)
				model()->setData(idx, !idx.data(TasksModel::ImportantRole).toBool(), TasksModel::ImportantRole);
			break;
		default:
			QTreeView::keyPressEvent(e);
			break;
	}
}

void TasksView::sortView()
{
	header()->setSortIndicator(0, Qt::DescendingOrder);
}

void TasksView::customContextMenuRequested(const QPoint& point )
{
	QModelIndex idx=indexAt(point);
	if(idx.isValid()) {
		QScopedPointer<QMenu> menu(new QMenu);
		
		bool isImportant=idx.data(TasksModel::ImportantRole).toBool();
		QString text;
		if(isImportant)
			text=tr("Not important");
		else
			text=tr("Mark as important");
		
		QAction* ratingAction=menu->addAction(TasksModel::ratingIcon(), text);
		QAction* removeAction=menu->addAction(QIcon::fromTheme( "list-remove" ), tr("Remove task"));
		removeAction->setEnabled(state()!=EditingState);
		
		QAction* res=menu->exec(QCursor::pos());
		if(res==ratingAction)
			model()->setData(idx, !isImportant, TasksModel::ImportantRole);
		else if(res==removeAction)
			model()->removeRow(idx.row());
		
	}
}

void TasksView::setModel(QAbstractItemModel* model)
{
	QTreeView::setModel(model);
	connect(model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), SLOT(sortView()));
	connect(this, SIGNAL(cleanDone()), model, SLOT(cleanDone()));
}

TasksModel* TasksView::tasksModel()
{
	return qobject_cast<TasksModel*>(model());
}

void TasksView::addTask()
{
	qDebug("lets add");
	setState(QAbstractItemView::NoState);
	
	QModelIndex idx=tasksModel()->addTask(QString(), false, false, QDateTime::currentDateTime().toTime_t());
	selectionModel()->clear();
	selectionModel()->setCurrentIndex(idx, QItemSelectionModel::Rows | QItemSelectionModel::Select);
	edit(idx);
}

#include "tasksview.moc"
