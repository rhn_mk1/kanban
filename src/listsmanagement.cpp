/*************************************************************************************                                                          
 *  Copyright (C) 2010 by Aleix Pol <aleixpol@kde.org>                               *                                                          
 *                                                                                   *                                                          
 *  This program is free software; you can redistribute it and/or                    *                                                          
 *  modify it under the terms of the GNU General Public License                      *                                                          
 *  as published by the Free Software Foundation; either version 2                   *                                                          
 *  of the License, or (at your option) any later version.                           *                                                          
 *                                                                                   *                                                          
 *  This program is distributed in the hope that it will be useful,                  *                                                          
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *                                                          
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *                                                          
 *  GNU General Public License for more details.                                     *                                                          
 *                                                                                   *                                                          
 *  You should have received a copy of the GNU General Public License                *                                                          
 *  along with this program; if not, write to the Free Software                      *                                                          
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *                                                          
 *************************************************************************************/

#include "listsmanagement.h"
#include <QDir>
#include <QDesktopServices>

QStringList ListsManagement::availableLists()
{
	QString path=QDesktopServices::storageLocation(QDesktopServices::DataLocation);
	QDir p(path);
	if(!p.exists()) {
		bool t=p.mkpath(path);
		Q_ASSERT(t);
	}
	
	QStringList ret=p.entryList(QDir::NoDotAndDotDot | QDir::Files);
	if(ret.isEmpty())
		ret.append(QObject::tr("default"));
	return ret;
}

QString ListsManagement::location(const QString& name)
{
	return QDesktopServices::storageLocation(QDesktopServices::DataLocation)+'/'+name;
}

QString ListsManagement::remoteCache(const QString& name)
{
	return location(QString())+"remote/"+name;
}

bool ListsManagement::removeList(const QString& name)
{
	return QFile::remove(ListsManagement::location(name));
}
