/*************************************************************************************                                                          
 *  Copyright (C) 2010 by Aleix Pol <aleixpol@kde.org>                               *                                                          
 *                                                                                   *                                                          
 *  This program is free software; you can redistribute it and/or                    *                                                          
 *  modify it under the terms of the GNU General Public License                      *                                                          
 *  as published by the Free Software Foundation; either version 2                   *                                                          
 *  of the License, or (at your option) any later version.                           *                                                          
 *                                                                                   *                                                          
 *  This program is distributed in the hope that it will be useful,                  *                                                          
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *                                                          
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *                                                          
 *  GNU General Public License for more details.                                     *                                                          
 *                                                                                   *                                                          
 *  You should have received a copy of the GNU General Public License                *                                                          
 *  along with this program; if not, write to the Free Software                      *                                                          
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *                                                          
 *************************************************************************************/

#ifndef MERGEDIALOG_H
#define MERGEDIALOG_H

#include <QtGui/QProgressDialog>
#include <QFile>
#include <QUrl>

#include "kanbanexport.h"

class QUrlInfo;
class QFtp;

class KANBAN_EXPORT MergeDialog : public QProgressDialog
{
	Q_OBJECT
	public:
		MergeDialog(const QUrl& url, QWidget* parent = 0);
		
	private slots:
		void performLocalSync();
		void commandFinished(int id, bool error);
		void listInfo(const QUrlInfo& item);
		void canceled();
		
	private:
		QFtp* m_remote;
		QList<QFile*> m_downloading;
		QUrl m_url;
		int m_uploads;
};

#endif // MERGEDIALOG_H
