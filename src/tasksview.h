/*************************************************************************************                                                          
 *  Copyright (C) 2009 by Aleix Pol <aleixpol@kde.org>                               *                                                          
 *                                                                                   *                                                          
 *  This program is free software; you can redistribute it and/or                    *                                                          
 *  modify it under the terms of the GNU General Public License                      *                                                          
 *  as published by the Free Software Foundation; either version 2                   *                                                          
 *  of the License, or (at your option) any later version.                           *                                                          
 *                                                                                   *                                                          
 *  This program is distributed in the hope that it will be useful,                  *                                                          
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *                                                          
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *                                                          
 *  GNU General Public License for more details.                                     *                                                          
 *                                                                                   *                                                          
 *  You should have received a copy of the GNU General Public License                *                                                          
 *  along with this program; if not, write to the Free Software                      *                                                          
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *                                                          
 *************************************************************************************/

#ifndef TASKSVIEW_H
#define TASKSVIEW_H

#include <QTreeView>
#include "kanbanexport.h"

class TasksModel;
class KANBAN_EXPORT TasksView : public QTreeView
{
	Q_OBJECT
	public:
		TasksView(QWidget* parent);
		
		void keyPressEvent(QKeyEvent* e);
		void mousePressEvent ( QMouseEvent * event );
		
		virtual void setModel(QAbstractItemModel* model);
		
	public slots:
		void addTask();
		
	private slots:
		void sortView();
		void customContextMenuRequested(const QPoint& point);
		 
	signals:
		void cleanDone();
		
	private:
		TasksModel* tasksModel();
};

#endif // TASKSVIEW_H
