/*************************************************************************************                                                          
 *  Copyright (C) 2009 by Aleix Pol <aleixpol@kde.org>                               *                                                          
 *                                                                                   *                                                          
 *  This program is free software; you can redistribute it and/or                    *                                                          
 *  modify it under the terms of the GNU General Public License                      *                                                          
 *  as published by the Free Software Foundation; either version 2                   *                                                          
 *  of the License, or (at your option) any later version.                           *                                                          
 *                                                                                   *                                                          
 *  This program is distributed in the hope that it will be useful,                  *                                                          
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *                                                          
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *                                                          
 *  GNU General Public License for more details.                                     *                                                          
 *                                                                                   *                                                          
 *  You should have received a copy of the GNU General Public License                *                                                          
 *  along with this program; if not, write to the Free Software                      *                                                          
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *                                                          
 *************************************************************************************/

#ifndef TASKSMODEL_H
#define TASKSMODEL_H

#include <QStandardItemModel>
#include "kanbanexport.h"

class KANBAN_EXPORT TasksModel : public QStandardItemModel
{
	Q_OBJECT
	public:
		enum TasksRole { ImportantRole=Qt::UserRole+3, TimestampRole };
		
		TasksModel(const QString& name, QObject *parent=0);
		virtual ~TasksModel();
		
		QModelIndex addTask(const QString& str, bool important, bool done, uint timestamp);
		bool save();
		bool load();
		
		void removeTasks(const QList<int>& idxs);
		
		uint lastSync() const;
		void markSynced();
		
		static QIcon ratingIcon();
		virtual bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);
		
	public slots:
		void cleanDone();
		
	private slots:
		void changesHappened();
		
	private:
		bool m_changes;
		QString m_path;
		bool m_holdSave;
		uint m_timestamp;
};

#endif // TASKSMODEL_H
