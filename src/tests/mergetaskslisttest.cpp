/*************************************************************************************                                                          
 *  Copyright (C) 2010 by Aleix Pol <aleixpol@kde.org>                               *                                                          
 *                                                                                   *                                                          
 *  This program is free software; you can redistribute it and/or                    *                                                          
 *  modify it under the terms of the GNU General Public License                      *                                                          
 *  as published by the Free Software Foundation; either version 2                   *                                                          
 *  of the License, or (at your option) any later version.                           *                                                          
 *                                                                                   *                                                          
 *  This program is distributed in the hope that it will be useful,                  *                                                          
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *                                                          
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *                                                          
 *  GNU General Public License for more details.                                     *                                                          
 *                                                                                   *                                                          
 *  You should have received a copy of the GNU General Public License                *                                                          
 *  along with this program; if not, write to the Free Software                      *                                                          
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *                                                          
 *************************************************************************************/
#define QT_GUI_LIB //What an ugly fix! :S

#include "mergetaskslisttest.h"
#include <QTest>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <taskslistmerge.h>
#include <QDomDocument>

QTEST_MAIN(MergeTasksListTest)

void MergeTasksListTest::mergeTest()
{
	QFETCH(QString, local);
	QFETCH(QString, remote);
	QFETCH(QString, result);
	
	QFile localFile("local");
	QFile remoteFile("remote");
	
	QVERIFY(localFile.open(QIODevice::WriteOnly | QIODevice::Text));
	QVERIFY(remoteFile.open(QIODevice::WriteOnly | QIODevice::Text));
	
	{
		QTextStream localStream(&localFile);
		QTextStream remoteStream(&remoteFile);
		localStream << local;
		remoteStream << remote;
		localFile.close();
		remoteFile.close();
	}
	
	TasksListMerge::merge("local", "remote");
	
	QVERIFY(localFile.open(QIODevice::ReadOnly | QIODevice::Text));
	QString obtained;
	{
		QTextStream localStream(&localFile);
		obtained=localStream.readAll();
		localFile.close();
	}
	
	bool eq=compareDOM(obtained, result);
	if(!eq)
		qDebug() << "error, got:" << endl << obtained << endl << "expected:" << endl << result;
	QVERIFY(eq);
}

void MergeTasksListTest::mergeTest_data()
{
	QTest::addColumn<QString>("local");
	QTest::addColumn<QString>("remote");
	QTest::addColumn<QString>("result"); //Rough-comparing

	QTest::newRow("no change")
										<< "<tasks timestamp='3'><task timestamp='2' important='true'>hi</task></tasks>"
										<< "<tasks timestamp='3'><task timestamp='2' important='true'>hi</task></tasks>"
										<< "<tasks              ><task timestamp='2' important='true'>hi</task></tasks>";
	QTest::newRow("remote addition")
										<< "<tasks timestamp='1' />"
										<< "<tasks timestamp='3'><task timestamp='2' important='true'>hi</task></tasks>"
										<< "<tasks              ><task               important='true'>hi</task></tasks>";
	
	QTest::newRow("remote removal")
										<< "<tasks timestamp='3'><task timestamp='2' important='true'>hi</task></tasks>"
										<< "<tasks timestamp='4' />"
										<< "<tasks timestamp='5' />";
	QTest::newRow("local addition")
										<< "<tasks timestamp='6'><task timestamp='6' important='true'>hi</task></tasks>"
										<< "<tasks timestamp='4' />"
										<< "<tasks              ><task timestamp='6' important='true'>hi</task></tasks>";
										
	QTest::newRow("local removal")
										<< "<tasks timestamp='20' />"
										<< "<tasks timestamp='4'><task timestamp='2' important='true'>hi</task></tasks>"
										<< "<tasks timestamp='8' />";
										
	QTest::newRow("importance changed")
										<< "<tasks timestamp='3'><task timestamp='2' important='false'>hi</task></tasks>"
										<< "<tasks timestamp='4'><task timestamp='2' important='true' >hi</task></tasks>"
										<< "<tasks              ><task timestamp='2' important='true' >hi</task></tasks>";
}

bool compareElements(const QDomElement& a, const QDomElement& b)
{
	bool ret=true;
	static const QStringList attrs=QStringList("important") << "done";
	
	foreach(const QString& attr, attrs)
		ret &= a.attribute(attr)==b.attribute(attr);
	
	ret &= a.text()==b.text();
	return ret;
}

//Just compares that the elements have the same element count and the same text body if no children
bool MergeTasksListTest::compareDOM(const QString& a, const QString& b)
{
	QDomDocument docA;
	Q_ASSERT(docA.setContent(a));
	QDomDocument docB;
	Q_ASSERT(docB.setContent(b));
	
	QDomElement ea=docA.documentElement(), eb=docB.documentElement();
	
	if(ea.childNodes().count()!=eb.childNodes().count())
		return false;
	
	bool ret=true;
	int i;
// 	qDebug() << "xeeee" << i << docA.toString() << docB.toString();
	for(i=0; ret && i<ea.childNodes().size(); i++) {
		ret &= compareElements(ea.childNodes().at(i).toElement(), eb.childNodes().at(i).toElement());
	}
	
	return ret;
}

#include "mergetaskslisttest.moc"
