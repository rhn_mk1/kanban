/*************************************************************************************                                                          
 *  Copyright (C) 2009 by Aleix Pol <aleixpol@kde.org>                               *                                                          
 *                                                                                   *                                                          
 *  This program is free software; you can redistribute it and/or                    *                                                          
 *  modify it under the terms of the GNU General Public License                      *                                                          
 *  as published by the Free Software Foundation; either version 2                   *                                                          
 *  of the License, or (at your option) any later version.                           *                                                          
 *                                                                                   *                                                          
 *  This program is distributed in the hope that it will be useful,                  *                                                          
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *                                                          
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *                                                          
 *  GNU General Public License for more details.                                     *                                                          
 *                                                                                   *                                                          
 *  You should have received a copy of the GNU General Public License                *                                                          
 *  along with this program; if not, write to the Free Software                      *                                                          
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *                                                          
 *************************************************************************************/

#include "tasks.h"

#include <QAction>
#include <QToolBar>
#include "tasksview.h"
#include "tasksmodel.h"
#include <QDesktopServices>
#include <QComboBox>
#include <QDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QSettings>
#include "mergedialog.h"
#include "listsmanagement.h"

Tasks::Tasks(QWidget* p)
	: QMainWindow(p)
{
	setWindowTitle(tr("Kanban"));
	setWindowIcon(QIcon::fromTheme("kanban"));
	
	m_view = new TasksView(this);
	setCentralWidget(m_view);
	
	m_model = new TasksModel(ListsManagement::location(tr("default")), this);
	m_view->setModel(m_model);
	
	QToolBar* tools=addToolBar(tr("Tasks"));
	QAction* a=tools->addAction(QIcon::fromTheme("list-add", QIcon::fromTheme("general_add")), tr("Add Task"), m_view, SLOT(addTask()));
	a->setShortcut(Qt::Key_Enter);
	tools->addAction(QIcon::fromTheme( "edit-clear", QIcon::fromTheme("general_stop") ), tr("Clean"), m_view, SIGNAL(cleanDone()));
	addToolBar(tools);
	
	m_lists=new QComboBox(this);
	m_lists->addItems(ListsManagement::availableLists());
	m_lists->setFocusPolicy(Qt::NoFocus);
	m_lists->setCurrentIndex(m_lists->findText(tr("default")));
	connect(m_lists, SIGNAL(currentIndexChanged(QString)), SLOT(changeCurrentList(QString)));
	
	QToolBar* lists=addToolBar(tr("Lists"));
	lists->addWidget(m_lists);
	lists->addAction(QIcon::fromTheme("document-new", QIcon::fromTheme("general_outbox")), tr("Add List"), this, SLOT(addList()));
	lists->addAction(QIcon::fromTheme("archive-remove", QIcon::fromTheme("general_sending_failed") ), tr("Remove List"), this, SLOT(removeList()));
	lists->addSeparator();
	m_syncAction=lists->addAction(QIcon::fromTheme("folder-sync", QIcon::fromTheme("general_synchronization")), tr("Synchronize"), this, SLOT(sync()));
	lists->addAction(QIcon::fromTheme("configure", QIcon::fromTheme("control_internet_setup")), tr("Configure"), this, SLOT(configureSync()));
	addToolBar(lists);
	
	m_settings=new QSettings(this);
	m_syncAction->setEnabled(remoteUrl().isValid());
}

Tasks::~Tasks()
{}

void Tasks::removeTask()
{
	QList<int> rows;
	foreach(const QModelIndex& idx, m_view->selectionModel()->selectedIndexes())
		rows.append(idx.row());
	
	m_model->removeTasks(rows);
}

void Tasks::addList()
{
	QString newName=QInputDialog::getText(this, tr("New list"), tr("What do you want the new list to be called?"));
	if(!newName.isEmpty()) {
		m_lists->addItem(newName);
		
		changeCurrentList(newName);
	}
}

void Tasks::changeCurrentList(const QString& newlist)
{
	if(newlist.isEmpty() || !m_view->model())
		return;
	
	QAbstractItemModel* m=m_model;
	m_model = new TasksModel(ListsManagement::location(newlist), this);
	m_view->setModel(m_model);
	
	int idx=m_lists->findText(newlist, Qt::MatchCaseSensitive);
	if(idx>=0) //if exists
		m_lists->setCurrentIndex(idx);
	else
		m_lists->setCurrentIndex(m_lists->findText(tr("default"), Qt::MatchCaseSensitive));
	
	delete m;
}

void Tasks::removeList()
{
	QString oldList=m_lists->currentText();
	int res=QMessageBox::warning(this, tr("Remove List"), tr("Are you sure you want to remove the %1 list?").arg(oldList), 
								 QMessageBox::Yes|QMessageBox::No);
	if(res==QMessageBox::Yes) {
		m_lists->removeItem(m_lists->currentIndex());
		
		ListsManagement::removeList(oldList);
	}
}

void Tasks::setRemoteUrl(const QUrl& url)
{
	m_syncAction->setEnabled(url.isValid());
	m_settings->setValue("syncUrl", url);
}

QUrl Tasks::remoteUrl()
{
	return m_settings->value("syncUrl").toUrl();
}

void Tasks::sync()
{
	QString selected=m_lists->currentText();
	m_model->save();
	
	MergeDialog d(remoteUrl());
	d.exec();
	
	m_lists->blockSignals(true);
	m_lists->clear();
	m_lists->blockSignals(false);
	m_lists->addItems(ListsManagement::availableLists());
	changeCurrentList(selected);
}

void Tasks::configureSync()
{
	QString res=QInputDialog::getText(this, tr("Kanban Sync Configuration"), tr("<qt>Enter the FTP folder you want to sync to:"
																				"<p/>Example: <em>ftp://username:password@ftp.mysite.org/mylists/path/</em></qt>"),
									  QLineEdit::Normal, remoteUrl().toString());
	if(!res.isEmpty()) {
		setRemoteUrl(QUrl(res));
	}
}

#include "tasks.moc"
