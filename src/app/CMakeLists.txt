
include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR})

set(kanban_SRCS tasks.cpp main.cpp)
qt4_automoc(${kanban_SRCS})

add_executable(kanban ${kanban_SRCS})
target_link_libraries(kanban kanbancommon)

install(TARGETS kanban RUNTIME DESTINATION bin)

if(DEFINED MAEMO)
	install(FILES kanban.desktop DESTINATION share/applications/hildon)
else(DEFINED MAEMO)
	install(FILES kanban.desktop DESTINATION share/applications)
endif(DEFINED MAEMO)
