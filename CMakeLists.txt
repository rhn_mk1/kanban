project(kanban)
cmake_minimum_required(VERSION 2.6)
find_package(Qt4 REQUIRED)
find_package(KDE4)

add_subdirectory(src)
add_subdirectory(icons)
